# PHYS 4200 Final Project

The stock price plot of a company is crucial for the understanding of the trends behind a companies valuation.  In this project I chose to analyze Facebooks stock over the past year and four months by breaking the stock price plot down into intervals of increasing and decreasing trends.  

On the increasing trends interval I further broke it down into sub-intervals where a line can be approximated to the data points to a high degree of precision.